
require([
    "esri/Map",
    "esri/views/MapView",
    "esri/request",
    "esri/layers/FeatureLayer",
    "esri/widgets/Legend",
    "esri/widgets/TimeSlider",
    "esri/widgets/Popup",
    "esri/widgets/Expand",
    "esri/widgets/LayerList",
    "esri/core/watchUtils",
    "esri/PopupTemplate",
    "esri/tasks/support/Query",
    "esri/views/layers/support/FeatureFilter",
    "esri/popup/content/support/ChartMediaInfoValueSeries",
    "esri/popup/content/support/ChartMediaInfoValue",
    "esri/layers/support/FeatureReductionCluster",
    "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js",
    "dojo/domReady!",

], function (Map, MapView, esriRequest, FeatureLayer, Legend, TimeSlider, Popup, Expand, LayerList, watchUtils, PopupTemplate, Query, FeatureFilter,ChartMediaInfoValueSeries, ChartMediaInfoValue, FeatureReductionCluster,Chart) {
    let features = [];
    let objectID = 1;
    let arrOfIds = [];
    let dataPoints = [];
    let vorigID = 0;
    var arrOfData = [];

    <!-- DATA FOR POPUP -->
    /* Create chart data array for the popup chart */
    if (arrOfData.length < 1) {
        $.getJSON("/mapdata", throughFarms);
        console.log("data array configured");
        console.log(arrOfData);
    }

    /* Loop through all the farm objects from JSON*/
    function throughFarms(data) {
        /* Add 1 to the total number of iterations*/
        for (let i = 0; i < (data.length + 1); i++) {
            /* Add new data to the arrOfData array */
            if (i < data.length){
                AddData(data[i]);
            /* At the end of the loop: Push the final dataPoint (last farm) to the data array (arrOfData)*/
            } else {
                arrOfData.push({
                    ID: vorigID, // FarmID
                    dataset: dataPoints  // Array of objects with Date and State of Infection of farms with same FarmID
                });
            }
        }
    }
    /* Add new data objects to the arrOfData array */
    function AddData(data) {
        /* Check ID, put multiple measurements with same ID on the same index. */
        if (!arrOfIds.includes(data.FarmID)) {
            arrOfIds.push(data.FarmID);

            if (vorigID !== 0) {
                /* Add new object to the arrOfData array*/
                arrOfData.push({
                    ID: vorigID, // FarmID
                    dataset: dataPoints // Array of objects with Date and State of Infection of farms with same FarmID
                });
                dataPoints = []; // empty the dataPoints array for the next FarmID
            }
        }
        /* Change y-axis from TRUE/FALSE to 0 or 1 */
        let st = 0; // Set FALSE to 0 for y-axis
        /* Change y-axis if the state of infection is TRUE*/
        if (/TRUE/.test("\'" + data.state + "\'") === true) {
            st = 1;
        }
        /* Create array with x and y data for chart */
        dataPoints.push({
            x: (new Date(data.date)), // Date of measurement
            y: st // State of infection (TRUE (1) & FALSE (0))
        });
        vorigID = data.FarmID; // set LastID to this FarmID
    }

    /*  Create a map */
    let map = new Map({
        basemap: "dark-gray"
    });

    /* Create a mapView */
    let view = new MapView({
        container: "viewDiv",
        map: map,
        center: [6.56667, 53.21917],
        zoom: 10,
        popup: {
            dockEnabled: true,
            dockOptions: {
                /* Disables the dock button from the popup. */
                buttonEnabled: false,
                breakpoint: false,
                position: "bottom-left"
            }
        }
    });

    /* Create a LayerList */
    const layerList = new LayerList({
        view: view,
        listItemCreatedFunction: function(event) {
            const item = event.item;
            if (item.layer.type !== "group") {
                // don't show legend twice
                item.panel = {
                    content: "legend",
                    open: true
                };
            }
        }
    });
    view.ui.add(layerList, "top-right");

    <!--  CREATE DATA OBJECTS -->
    /* Options for the esri request */
    let options = {
        query: {
            limit: 30
        },
        responseType: "json"
    };

    /* Get data from /mapdata page and create the data points */
    esriRequest("/mapdata", options).then(function (response) {
        let farms = JSON.stringify(response, null, 2);
        response.data.forEach(farm => {

            // Extract data
            let geometry = {
                type: "point",
                x: parseFloat(farm['longitude']),
                y: parseFloat(farm['latitude'])
            };

            let attr = {
                ObjectID: objectID,
                FarmID: farm.FarmID,
                date: farm.date,
                state: farm.state,
                latitude: farm.latitude,
                longitude: farm.longitude
            };

            features.push({
                geometry: geometry,
                attributes: attr
            });

            objectID++;
        });

        /* Feature fields */
        const fields = [
            {
                name: "ObjectID",
                alias: "ObjectID",
                type: "oid"
            }, {
                name: "state",
                alias: "state",
                type: "string"
            }, {
                name: "FarmID",
                alias: "FarmID",
                type: "string"
            }, {
                name: "date",
                alias: "date",
                type: "string"
            }, {
                name: "latitude",
                alias: "latitude",
                type: "string"
            }, {
                name: "longitude",
                alias: "longitude",
                type: "string"
            }];

        <!-- CLUSTERED FEATURE LAYER -->
        /* Create a feature layer with every data point*/
        // renderer
        const allRenderer = {
            type: "simple", // autocasts as new SimpleRenderer()
            objectIdField: "ObjectID",
            label: "Investigated farm",
            field: "FarmID",
            symbol: {
                type: "simple-marker", // autocasts as new SimpleMarkerSymbol()
                size: 9,
                color: [0, 123, 255],
                outline: null
            }
        };

        // Feature Layer
        const allLayer = new FeatureLayer({
            source: features,
            objectIdField: "ObjectID",
            renderer: allRenderer,
            title: "Every investigated farm",
            fields: fields,
            visible: false
        });

        map.add(allLayer);

         // Cluster the points
        allLayer.featureReduction = {
            type: "cluster",
            // clusterRadius: "2px",
            popupTemplate: {
                content: "This cluster represents {cluster_count} data points."
            }
        };

        <!-- FEATURE LAYER  WITH UNIQUE VALUES -->
         /* Symbol for state of infection*/
        const contaminated = { // Red dots when infected with Salmonella
            type: "simple-marker",  // autocasts as new PictureMarkerSymbol()
            size: 12,
            color: [255, 0, 0, 0.60],
            outline: {  // autocasts as new SimpleLineSymbol()
                color: [80, 20, 20, 0.10 ],
                width: "3px"
            }
        };

        /* Symbol for state of infection*/
        const clean = { // Gray dots when current farm is not infected
            type: "simple-marker",  // autocasts as new PictureMarkerSymbol()
            size: 10,
            color: null,
            outline: {  // autocasts as new SimpleLineSymbol()
                color: [ 128, 128, 128, 0.1 ],
                width: "0.5px"
            }
        };

        /* Create Feature Layer with unique value infos*/
        // Renderer
        const contaminationRenderer = {
            type: "unique-value", // autocasts as new UniqueValueRenderer()
            legendOptions: {
                title: "State of contamination"
            },
            field: "state",
            uniqueValueInfos: [
                {
                    value: "TRUE", // code for infected
                    symbol: contaminated,
                    label: "Contaminated with Salmonella"
                },
                {
                    value: "FALSE", // code for non-infected
                    symbol: clean,
                    label: "Clean tank"
                }
            ]
        };

        // Feature Layer
        const layer = new FeatureLayer({
            source: features,
            objectIdField: "ObjectID",
            renderer: contaminationRenderer,
            fields: fields,
            title: "Current state of infection",
            popupEnabled: true,
            outFields: ["*"],
            definitionExpression: "date",
            timeInfo: {
                startField: "date", // name of the date field
                // interval: { // specify time interval for
                //     unit: "years",
                //     value: 1
                // }
            }
        });

        // Add FeatureLayer to the map.
        map.add(layer);

        <!-- POPUP -->
        layer.popupTemplate = {
            // autocasts as new PopupTemplate()
            title: '<div class="d-flex flex-row flex-wrap popup-title-container">' +
                '<div class="d-flex">Farm ID: {FarmID} </div>',
            content: getPopupContent, // Generate popup content int the functions below
            fieldInfos: [{
                fieldName: "date",
            }, {
                fieldName: "state",
            }],
            featureNavigationEnabled: false
        }; // Add popuptemplate to layer
        // this.map.popupTemplate.resize(360,280);





        <!-- LEGEND -->
        const legend = new Legend({
            view: view,
            container: document.createElement("infoDiv"),
            layerInfos: [
                {
                    layer: layer,
                    title: "Infection of Salmonella at Farms (Groningen)"
                },
                {
                    layer: allLayer,
                    title: "Infection of Salmonella at Farms (Groningen)"
                }
            ]
        });

        // Make the legend expandable
        let bgExpand = new Expand({
            view: view,
            content: legend,
            expandTooltip: "Legend",
            expanded: true
        });

        // view.ui.add(bgExpand, "top-right");


        <!-- TIME SLIDER -->
        const timeSlider = new TimeSlider({
            container: "timeSliderDiv",
            playRate: 1000,
            stops: {
                interval: {
                    value: 4,
                    unit: "months"
                }
            }
        });
        view.ui.add(timeSlider, "bottom");

        let layerView;

        view.whenLayerView(layer).then(function (lv) {
            layerView = lv;
            const fullTimeExtent = layer.timeInfo.fullTimeExtent;
            // start time of the time slider - 1/4/2017
            const start = new Date(2017, 4, 1);
            // set time slider's full extent to from start until end date of layer's fullTimeExtent
            timeSlider.fullTimeExtent = {
                start: start,
                end: layer.timeInfo.fullTimeExtent.end
            };

            const end = new Date(start);
            // end of current time extent for time slider showing farms with 4 months interval
            end.setDate(end.getDate() + 120);

            // Values property is set so that time slider widget show a third of a year.
            // We are setting the thumbs positions.
            timeSlider.values = [start, end];
        });


        // watch for time slider timeExtent change
        timeSlider.watch("timeExtent", function () {
            // only show farms up until the end of timeSlider's current time extent.
            layer.definitionExpression =
                "date <= " + timeSlider.timeExtent.end.getTime();

            // now gray out farms that happened before the time slider's current
            // timeExtent... leaving footprint of farms that already happened
            layerView.effect = {
                filter: {
                    timeExtent: timeSlider.timeExtent,
                    geometry: view.extent
                },
                excludedEffect: "opacity(20%)",
                includedEffect: "brightness(150%) opacity(80%)"
            };
        });


        /* Generate the Content for the popup, return html div*/
        function getPopupContent(feature) {
            /* Get attributes from selected feature. */
            let attributes = feature.graphic.attributes;
            let content = document.createElement('div');
            /* Horizontal breaks. */
            var hr = document.createElement('HR');

            /* Generate content rows and append them to content div. */
            content.append(getPopupSummary(attributes));
            content.append(hr);
            content.append(getPopupChart(attributes));
            // content.append(hr2);
            // content.append(getGiraffePopupList(attributes));

            return content;
        }

        /* Create a html div with info about the data point*/
        function getPopupSummary(attributes){
            /* Create HTML Layout of the summary. */
            var row = document.createElement('div');
            var col1 = document.createElement('div');
            var col2 = document.createElement('div');

            row.className = "row";
            col1.className = "col-6"; col2.className = "col-6";

            /* Fill columns with sighting attributes. */
            col1.innerHTML = "<span><b>Date: </b>" + new Date(attributes.date).toDateString() +
                "</span><br><span><b>Current State: </b>" + attributes.state +
                "</span><br><span><b>Number of measurements: </b>" + getRightData(attributes.FarmID).length +
                "</span>";

            col2.innerHTML = "<span><b>Longitude: </b><small>" + attributes.longitude +
                "</small></span><br><span><b>Latitude: </b><small>" + attributes.latitude +
                "</small></span>";

            /* Add columns to row. */
            row.appendChild(col1); row.appendChild(col2);

            return row

        }

        /* Generate a line chart with the historic overview of infections for the popup*/
        function getPopupChart(attributes) {
            /* Create row container and column for chart title and canvas. */
            var row = document.createElement('div');
            var col = document.createElement('div');
            /* Create canvas for chart */
            var canvas = document.createElement('canvas');

            row.className = "row justify-content-left";
            col.className = "col-lg-10 popup-chart-container";

            /* Create and add title to row as span element. */
            row.innerHTML = '<span><b> Historic overview of contamination at Farm nr. </b><small>' + attributes.FarmID + "</small></span>";

            /* Get the right data set from arrOfData array*/
            let result = getRightData(attributes.FarmID);

            result.sort(dynamicSort("x"));

            /* Create chart using Chart.js and attributes values. */
            let myChart = new Chart(canvas, {
                type: 'line',
                data: {
                    // labels: ['Months'],
                    datasets: [{
                        label: 'Milk bulktank',
                        data: result,
                        backgroundColor:
                        [
                            'rgba(255, 99, 132, 0.2)'
                        ],
                        borderColor:
                            [
                            'rgba(255, 99, 132, 1)'
                        ],
                        borderWidth: 1,
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                callback: function(value, index, values) {
                                    if (value=== 1){
                                        return  "Contaminated Tank";}
                                    if (value=== 0){
                                        return  "Clean Tank";}
                                }
                            }
                        }],
                        xAxes: [{
                            type: "time",
                            time: {
                                unit: "month",
                                displayFormats: {
                                    month: 'MMM YYYY'
                                }
                            }
                        }]

                    }
                }
            });




            col.appendChild(canvas);
            row.appendChild(col);

            return row;
        }

        /* Get the dataset belonging to the given FarmID */
        function getRightData(farmid) {
            let rightData = [];
            /* Check if FarmID matches object ID*/
            let data = arrOfData.filter(obj => {
                if (obj.ID.toString() === farmid.toString()) {
                    // set the dataset as the rightData variable
                    rightData = obj.dataset;
                }
            });
            return rightData; // Return correct dataset
        }

        function dynamicSort(property) {
            var sortOrder = 1;
            if(property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a,b) {
                /* next line works with strings and numbers,
                 * and you may want to customize it to your needs
                 */
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            }
        }
    });
});




