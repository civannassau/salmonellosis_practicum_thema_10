package servlets;

/*
 * @author Jildou Haaijer
 * @version 0.0.1
 */

import service.FarmDataFetcher;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet is created to have a mapdata page to check how the data looks like.
 * It contains of a doGet method.
 */

@WebServlet(name = "FarmDataServlet", urlPatterns = "/mapdata")
public class FarmDataServlet extends HttpServlet {

    // doGet method
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        FarmDataFetcher fetcher = new FarmDataFetcher();
        String json = null;
        // try catch to add json into fetcher.getFarms
        try {
            json = fetcher.getFarms();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // gives json data on /mapdata
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }
}