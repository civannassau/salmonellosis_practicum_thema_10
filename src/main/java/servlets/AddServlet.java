package servlets;

/*
 * @author Jildou Haaijer
 * @version 0.0.1
 */

import config.WebConfig;
import org.thymeleaf.context.WebContext;
import service.DatabaseDataInserter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * This servlet is created for the add data page, where it is possible to add new data
 * as a single instance or csv file. The methods are doGet and doPost.
 */
@WebServlet(name = "AddServlet", urlPatterns = "/Add", loadOnStartup = 1)
public class AddServlet extends HttpServlet {
    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = this.getServletContext();
        WebConfig.createTemplateEngine(servletContext);
    }

    // doPost method
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // Strings requested
        String id = request.getParameter("farm");
        String latitude = request.getParameter("latitude");
        String longitude = request.getParameter("longitude");
        String date = request.getParameter("selected_date");
        String state = request.getParameter("contamination");

        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);

        // If strings are not equal to 0 it will be inserted in db eventually
        if (id != null | latitude != null | longitude != null | date != null | state != null) {

            // changes yyyy-MM-dd to dd-MM-yyyy
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = null;
            try {
                d = sdf.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            sdf.applyPattern("dd/MM/yyyy");
            String newDateString = "\"" + sdf.format(d) + "\"";


            // Prints strings of datapoints which are inserted
            System.out.println("Datapoint inserted....=" + id  + " "+ latitude+  " "+longitude  + " "+ newDateString +" "+ state);
            // Inserts data in db
            DatabaseDataInserter.InsertData(id, latitude, longitude, newDateString, state);
            ctx.setVariable("dataApproved", "Data point has been added");

        } else System.out.println(new NullPointerException("Values are null"));

        final ServletContext servletContext = super.getServletContext();
        WebConfig.createTemplateEngine(servletContext).process("Add", ctx, response.getWriter());

    }

    // doGet method
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);
            final ServletContext servletContext = super.getServletContext();
            WebConfig.createTemplateEngine(servletContext).process("Add", ctx, response.getWriter());
        }
    }

