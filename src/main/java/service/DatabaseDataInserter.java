package service;

/*
 * @author Jildou Haaijer
 * @version 0.0.1
 */

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * This class had is used to insert new data into the database. It contains the following methods: InsertData,
 * InsertFile and RemoveDuplicates. InsertData adds a single row to the Database. InsertFile adds data from a
 * csvfile to the database. RemoveDuplicates removes the duplicate rows from the database.
 */
public class DatabaseDataInserter {

    /**
     * InsertData adds a new row to the database.
     * @param id The Farm ID of the object
     * @param lat The latitude of the object
     * @param lon The longitude of the object
     * @param date The date on which the sample was taken
     * @param state The state of contamination. T (True) when the sample contained salmonella or F (False when it
     *              did not contain salmonella.)
     */
    public static void InsertData(String id, String lat, String lon, String date, String state) {
        Connection conn = null;
        Statement stmt = null;

        try{
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.cj.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to a selected database...");
            conn = SQLconnector.initializeDatabase();
            System.out.println("Connected database successfully...");

            //STEP 4: Execute a query
            System.out.println("Inserting records into the table...");
            stmt = conn.createStatement();

            // Create farms table if it does not exist
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS salmonellosis.Farms (\n" +
                    "                                     FarmID VARCHAR(5),\n" +
                    "                                     Latitude VARCHAR(20),\n" +
                    "                                     Longitude VARCHAR(20),\n" +
                    "                                     IDate VARCHAR(50),\n" +
                    "                                     Contamination CHAR(1)\n" +
                    ");");
            stmt.executeUpdate("INSERT INTO salmonellosis.Farms(FarmID,Latitude,Longitude,IDate,Contamination) " +
                    "VALUE ('"+id+"','"+lat+"','"+lon+"',"+ date+",'"+state.charAt(0)+"')");
            System.out.println("Inserted records into the table...");

            // Remove duplicate rows from database
            RemoveDuplicates(stmt);

        } catch(Exception se){
            //Handle errors for JDBC
            se.printStackTrace();
        }//Handle errors for Class.forName
        finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    conn.close();
            }catch(SQLException ignored){
            }// do nothing
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
    }//end main,


    /**
     * This method inserts the data from a csvfile into the database.
     * @param csvfile A csvfile with FarmID, Latitude, Longitude, Date and State data.
     */
    public static void InsertFile(String csvfile) {
        Connection conn = null;
        Statement stmt = null;
        try{
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.cj.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to a selected database...");
            conn = SQLconnector.initializeDatabase();
            System.out.println("Connected database successfully...");

            //STEP 4: Execute a query
            System.out.println("Inserting records into the table...");
            stmt = conn.createStatement();

            // Create table if it does not exist
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS salmonellosis.Farms (\n" +
                    "                                     FarmID VARCHAR(5),\n" +
                    "                                     Latitude VARCHAR(20),\n" +
                    "                                     Longitude VARCHAR(20),\n" +
                    "                                     IDate VARCHAR(50),\n" +
                    "                                     Contamination CHAR(1)\n" +
                    ");");
//            stmt.execute("SET GLOBAL local_infile = 1;")

            // Load csvfile into database
            stmt.executeUpdate(" LOAD DATA LOCAL INFILE '" + csvfile +
                    "' INTO TABLE salmonellosis.Farms " +
                    " FIELDS TERMINATED BY \';\' ENCLOSED BY \'\"'" +
                    " LINES TERMINATED BY \'\\n\'" +
                    " IGNORE 1 LINES; ");


            System.out.println("Inserted records into the table...");

            // remove duplicate rows from the table
            RemoveDuplicates(stmt);

        } catch(Exception se){
            //Handle errors for JDBC
            se.printStackTrace();
        }//Handle errors for Class.forName
        finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    conn.close();
            }catch(SQLException ignored){
            }// do nothing
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
    }


    /**
     * This method removes the duplicates all the duplicate rows from the table.
     * @param stmt database table to remove the duplicates from
     */
    private static void RemoveDuplicates(Statement stmt){
        try{
            // Remove Duplicates from table
            System.out.println("Removing duplicates from the table...");

            // Create a new table (a copy of the farms table)
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS salmonellosis.Farms_copy LIKE salmonellosis.Farms;");

            // Add every row that has a unique Farmid and Date to the copy table
            // (Thereby removing every row with duplicate dates)
            stmt.executeUpdate("INSERT INTO salmonellosis.Farms_copy" +
                    " SELECT * FROM salmonellosis.Farms" +
                    " GROUP BY salmonellosis.Farms.FarmID, salmonellosis.Farms.IDate; ");

            // Delete the farms table
            stmt.executeUpdate("DROP TABLE salmonellosis.Farms;");

            // Rename the copy table to Farms
            stmt.executeUpdate("ALTER TABLE salmonellosis.Farms_copy RENAME TO salmonellosis.Farms;");

            System.out.println("removed duplicates from the table...");

        }catch(Exception se){
            //Handle errors for JDBC
            se.printStackTrace();
        }
        System.out.println("Goodbye!");
    }
}
