package service;

/*
 * @author Jildou Haaijer
 * @version 0.0.1
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class SQLconnector {

    /**
     * @return the database connection
     * @throws SQLException in case the connection fails
     * @throws ClassNotFoundException in case the class is not found
     */
    static Connection initializeDatabase()
            throws SQLException, ClassNotFoundException
    {
        // Initialize all the information regarding
        // Database Connection
        String dbDriver = "com.mysql.cj.jdbc.Driver";
        String dbURL = "jdbc:mysql://localhost:3306/salmonellosis?allowLoadLocalInfile=true";
        // Database name to access
//        String dbName = "salmonellosis";
        String dbUsername = "salmonellosis" ;
        String dbPassword = "pn5Tt3Ta";

        Class.forName(dbDriver);
        return DriverManager.getConnection(dbURL,
                dbUsername,
                dbPassword);
    }
}
